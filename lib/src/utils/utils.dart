import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showAlert(
    BuildContext context, String mensaje, String title, String image) {
  final size = MediaQuery.of(context).size;
  showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.01),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage(image),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              Text(
                title,
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height * 0.01),
            ],
          ),
          content: Text(
            mensaje.replaceAll('<b>', '').replaceAll('</b>', ''),
            textAlign: TextAlign.center,
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Entendido',
                style: TextStyle(
                    color: Color.fromRGBO(60, 140, 116, 1),
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      });
}

void showAlert2(
    BuildContext context, String mensaje, String title, String image) {
  final size = MediaQuery.of(context).size;
  showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Column(
            children: <Widget>[
              SizedBox(height: size.height * 0.01),
              Container(
                height: size.height * 0.16,
                child: Image(
                  image: AssetImage(image),
                ),
              ),
              SizedBox(height: size.height * 0.01),
              Text(
                "Acerca de DessertApp",
                style: TextStyle(),
                textAlign: TextAlign.center,
              ),
              SizedBox(height: size.height * 0.01),
            ],
          ),
          content: Column(
            children: <Widget>[
              Text(
                'App para la compra y venta de alimentos',
                style: Theme.of(context).textTheme.headline6,
              ),
              Text(''),
              Text('Jessica Andrea Garcia'),
              Text('Jennyfer Jackeline Fuentes'),
              Text('Corporacion Universitaria Adventista'),
              Text('Programanción Movíl'),
              Text('Medellin-Antioquia 2020'),
            ],
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'Entendido',
                style: TextStyle(
                    color: Color.fromRGBO(60, 140, 116, 1),
                    fontWeight: FontWeight.w600),
                textAlign: TextAlign.center,
              ),
              onPressed: () => Navigator.of(context).pop(),
            ),
          ],
        );
      });
}


void loadings(BuildContext context) {
  final size = MediaQuery.of(context).size;
  CupertinoAlertDialog alert = CupertinoAlertDialog(
      content: Container(
          height: size.height * 0.25, child: CupertinoActivityIndicator()));

  showCupertinoDialog(
      // barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      });
}
