import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';
import 'package:flutter/material.dart';

class DetailsPage extends StatefulWidget {
  const DetailsPage({Key key}) : super(key: key);

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  Widget build(BuildContext context) {
    Size s = MediaQuery.of(context).size;

    final Map producto = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(228, 214, 124, 1),
        title: Text(
          'jenny',
          style: TextStyle(color: Color.fromRGBO(27, 28, 28, 1)),
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/cactus(1).png'),
          )
        ],
      ),
      body: Column(
        children: <Widget>[
          Container(
            alignment: Alignment.center,
            height: s.height * 0.3,
            width: s.width,
            child: Image.network(
              producto['foto'],
              fit: BoxFit.contain,
            ),
          ),
          Container(
            margin: EdgeInsets.only(top: 10),
            width: s.width * 0.9,
            color: Colors.grey,
            height: s.height * 0.001,
          ),
          Container(
            height: s.height * 0.07,
            alignment: Alignment.center,
            child: Text(
              producto['nombre'],
              style: Theme.of(context).textTheme.headline4,
            ),
          ),
          Container(
            // margin: EdgeInsets.only(top: 10),
            width: s.width * 0.9,
            color: Colors.grey,
            height: s.height * 0.001,
          ),
          Container(
            padding: EdgeInsets.all(20),
            alignment: Alignment.topLeft,
            height: s.height * 0.41,
            child: Text(
              producto['descripcion'],
              style: Theme.of(context)
                  .textTheme
                  .bodyText2
                  .apply(fontSizeFactor: 1.1),
            ),
          ),
          Container(
            // margin: EdgeInsets.only(top: 10),
            width: s.width * 0.9,
            color: Colors.grey,
            height: s.height * 0.001,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(8),
                      child: Text(
                        'Precio:',
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .apply(fontSizeFactor: 0.9),
                      ),
                    ),
                    Container(
                      color: Colors.black12,
                      padding: EdgeInsets.all(5),
                      child: Text('${producto["precio"]}',
                          style: Theme.of(context)
                              .textTheme
                              .headline6
                              .apply(fontSizeFactor: 0.9)),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: s.width * 0.09,
              ),
              FlatButton(
                  color: Color.fromRGBO(107, 152, 71, 1),
                  onPressed: () {
                    Navigator.pushNamed(context, 'pedido',
                        arguments: producto['id'].toString());
                  },
                  child: Text('Comprar',
                      style: Theme.of(context)
                          .textTheme
                          .headline6
                          .apply(fontSizeFactor: 0.9)))
            ],
          ),
          Container(
            // margin: EdgeInsets.only(top: 10),
            width: s.width * 0.9,
            color: Colors.grey,
            height: s.height * 0.001,
          ),
        ],
      ),
    );
  }
}
