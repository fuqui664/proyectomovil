import 'package:jenny/src/utils/utils.dart';
import 'package:flutter/material.dart';

class InitPage extends StatelessWidget {
  const InitPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de DessertApp", "assets/cactus(1).png");
          }),
      backgroundColor: Color.fromRGBO(228, 214, 124, 1),
      body: SafeArea(
        child: Center(
            child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Container(
              margin: EdgeInsets.fromLTRB(0, 0, 0, size.height * 0.35),
              child: Image.asset('assets/tenor.gif'),
            ),
            Center(
              child: Container(
                margin: EdgeInsets.fromLTRB(0, size.height * 0.2, 0, 0),
                decoration: BoxDecoration(
                    // color: Color.fromRGBO(107, 152, 71, 0.6),
                    borderRadius: BorderRadius.circular(10)),
                height: size.height * 0.21,
                width: size.width * 0.45,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                      onPressed: () {
                        Navigator.of(context).pushNamed('login');
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: size.width * 0.9,
                        height: size.height * 0.06,
                        child: Text(
                          'Iniciar sesion',
                          style: TextStyle(
                            color: Color.fromRGBO(27, 28, 28, 1),
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                      color: Color.fromRGBO(107, 152, 71, 1),
                    ),
                    FlatButton(
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0)),
                      onPressed: () {
                        Navigator.pushNamed(context, 'registro');
                      },
                      child: Container(
                        alignment: Alignment.center,
                        width: size.width * 0.9,
                        height: size.height * 0.06,
                        child: Text(
                          'Registrarse',
                          style: TextStyle(
                            color: Color.fromRGBO(27, 28, 28, 1),
                            fontWeight: FontWeight.bold,
                            fontSize: 16.0,
                          ),
                        ),
                      ),
                      color: Color.fromRGBO(107, 152, 71, 1),
                    )
                  ],
                ),
              ),
            )
          ],
        )),
      ),
    );
  }
}
