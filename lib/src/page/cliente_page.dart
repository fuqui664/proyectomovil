import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/utils/utils.dart';

extension StringExtension on String {
  String capitalize() {
    return "${this[0].toUpperCase()}${this.substring(1)}";
  }
}

class ClientePage extends StatefulWidget {
  ClientePage({Key key}) : super(key: key);

  @override
  _ClientePageState createState() => _ClientePageState();
}

class _ClientePageState extends State<ClientePage> {
  Size s;
  String search = '';
  List results;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    return Scaffold(
        floatingActionButton: IconButton(
            icon: Icon(Icons.code),
            onPressed: () {
              showAlert2(
                  context, "", "Acerca de DessertApp", "assets/cactus(1).png");
            }),
        appBar: AppBar(
          elevation: 8,
          backgroundColor: Color.fromRGBO(228, 214, 124, 1),
          title: Text(
            'DessertApp',
            style: TextStyle(color: Color.fromRGBO(27, 28, 28, 1)),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset('assets/cactus(1).png'),
            )
          ],
          bottom: PreferredSize(
            preferredSize: Size(s.width, s.height * 0.085),
            child: Container(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 4),
                color: Colors.white,
                child: TextField(
                  onChanged: (value) async {
                    if (value.length > 4) {
                      setState(() {
                        search = value;
                      });
                    }
                    if (value.length == 0) {
                      setState(() {
                        search = value;
                      });
                    }
                  },
                  decoration: InputDecoration(
                      suffixIcon: Icon(Icons.search),
                      hintText: ' Buscar',
                      hintStyle: TextStyle(fontSize: 18)),
                )),
          ),
        ),
        body: search == ''
            ? FutureBuilder<List>(
                future: DBProvider.db.getResults(),
                // initialData: InitialData,
                builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CupertinoActivityIndicator(),
                    );
                  }
                  results = snapshot.data;
                  if (results.length == 0 || search != '') {
                    return Column(
                      // crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        SizedBox(
                          height: s.height * 0.03,
                        ),
                        Center(
                          child: Card(
                            margin: EdgeInsets.all(15),
                            child: Container(
                                alignment: Alignment.center,
                                height: s.height * 0.765,
                                child: Text("No hay resultados")),
                          ),
                        )
                      ],
                    );
                  }
                  return SafeArea(
                    child: Column(
                      children: <Widget>[
                        Flexible(
                          child: ListView.builder(
                            itemBuilder: (context, i) {
                              return GestureDetector(
                                child: Card(
                                    elevation: 5,
                                    margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(
                                          // height: s.height * 0.1,
                                          width: s.width * 0.2,
                                          padding: EdgeInsets.all(8),
                                          child:
                                              Image.network(results[i]["foto"]),
                                        ),
                                        Container(
                                          width: s.width * 0.005,
                                          color: Colors.grey,
                                          height: s.height * 0.1,
                                        ),
                                        Column(
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  padding: EdgeInsets.all(8),
                                                  child: Text(
                                                    results[i]['descripcion'],
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            27, 28, 28, 1)),
                                                  ),
                                                  width: s.width * 0.55,
                                                ),
                                              ],
                                            ),
                                            Container(
                                              width: s.width * 0.55,
                                              color: Colors.grey,
                                              height: s.height * 0.001,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                SizedBox(
                                                  width: s.width * 0.01,
                                                ),
                                                Container(
                                                    padding: EdgeInsets.all(5),
                                                    child: Text('Precio')),
                                                Container(
                                                  color: Colors.black12,
                                                  padding: EdgeInsets.all(5),
                                                  child: Text(
                                                      '${results[i]["precio"]}'),
                                                ),
                                                SizedBox(
                                                  width: s.width * 0.09,
                                                ),
                                                FlatButton(
                                                    color: Color.fromRGBO(
                                                        107, 152, 71, 1),
                                                    onPressed: () {
                                                      Navigator.pushNamed(
                                                          context, 'pedido',
                                                          arguments: results[i]
                                                                  ['id']
                                                              .toString());
                                                    },
                                                    child: Text('Comprar'))
                                              ],
                                            ),
                                          ],
                                        )
                                      ],
                                    )),
                                onTap: () {
                                  Navigator.pushNamed(context, 'detalles',
                                      arguments: results[i]);
                                },
                              );
                            },
                            itemCount: results.length,
                          ),
                        )
                      ],
                    ),
                  );
                },
              )
            : busqueda());
  }

  Widget busqueda() {
    List<Map> temp = [];
    results.forEach((element) {
      if (element['nombre'].toString().contains(search) ||
          element['nombre'].toString().contains(search.capitalize()) ||
          element['descripcion'].toString().contains(search) ||
          element['descripcion'].toString().contains(search.capitalize())) {
        temp.add(element);
      }
    });
    if (temp.length == 0) {
      return Column(
        // crossAxisAlignment: CrossAxisAlignment.center,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          SizedBox(
            height: s.height * 0.03,
          ),
          Center(
            child: Card(
              margin: EdgeInsets.all(15),
              child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.735,
                  child: Text("No hay resultados")),
            ),
          )
        ],
      );
    }
    return Column(
      children: <Widget>[
        Flexible(
          child: ListView.builder(
            itemBuilder: (context, i) {
              return GestureDetector(
                child: Card(
                    elevation: 5,
                    margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Container(
                          // height: s.height * 0.1,
                          width: s.width * 0.2,
                          padding: EdgeInsets.all(8),
                          child: Image.network(temp[i]["foto"]),
                        ),
                        Container(
                          width: s.width * 0.005,
                          color: Colors.grey,
                          height: s.height * 0.1,
                        ),
                        Column(
                          children: <Widget>[
                            Row(
                              children: <Widget>[
                                Container(
                                  padding: EdgeInsets.all(8),
                                  child: Text(
                                    temp[i]['descripcion'],
                                    style: TextStyle(
                                        color: Color.fromRGBO(27, 28, 28, 1)),
                                  ),
                                  width: s.width * 0.55,
                                ),
                              ],
                            ),
                            Container(
                              width: s.width * 0.55,
                              color: Colors.grey,
                              height: s.height * 0.001,
                            ),
                            Row(
                              children: <Widget>[
                                SizedBox(
                                  width: s.width * 0.01,
                                ),
                                Container(
                                    padding: EdgeInsets.all(5),
                                    child: Text('Precio')),
                                Container(
                                  color: Colors.black12,
                                  padding: EdgeInsets.all(5),
                                  child: Text('${temp[i]["precio"]}'),
                                ),
                                SizedBox(
                                  width: s.width * 0.09,
                                ),
                                FlatButton(
                                    color: Color.fromRGBO(107, 152, 71, 1),
                                    onPressed: () {
                                      Navigator.pushNamed(context, 'pedido',
                                          arguments: temp[i]['id'].toString());
                                    },
                                    child: Text('Comprar'))
                              ],
                            ),
                          ],
                        )
                      ],
                    )),
                onTap: () {
                  Navigator.pushNamed(context, 'detalles', arguments: temp[i]);
                },
              );
            },
            itemCount: temp.length,
          ),
        )
      ],
    );
  }
}
