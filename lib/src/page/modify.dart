
import 'package:flutter/material.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';

class ModifyPage extends StatefulWidget {
  ModifyPage({Key key}) : super(key: key);

  @override
  _ModifyPageState createState() => _ModifyPageState();
}

class _ModifyPageState extends State<ModifyPage> {
  String descripcion = '';
  String nombre = '';
  String foto = '';
  String precio = '';
  final UserPrefs prefs = new UserPrefs();
  Size s;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    // final id_producto = ModalRoute.of(context).settings.arguments;
    final Map f = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de DessertApp", "assets/cactus(1).png");
          }),
      backgroundColor: Color.fromRGBO(228, 214, 124, 1),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Container(
              height: s.height,
              child: Column(
                children: <Widget>[
                  Image.asset('assets/tenor.gif'),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: f['nombre'], helperText: 'Nombre'),
                      onChanged: (value) {
                        nombre = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: f['descripcion'],
                          helperText: 'Descripción'),
                      onChanged: (value) {
                        descripcion = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: f['foto'], helperText: 'Imagen'),
                      onChanged: (value) {
                        foto = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          hintText: f['precio'].toString(),
                          helperText: 'Precio'),
                      onChanged: (value) {
                        precio = value;
                      },
                    ),
                  ),
                  SizedBox(
                    height: s.height * 0.03,
                  ),
                  Container(
                    width: s.width * 0.5,
                    height: s.height * 0.07,
                    child: RaisedButton(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: s.width * 0.1,
                              vertical: s.height * 0.01),
                          //texto que se muestra en el boton
                          child: Text(
                            'Modificar',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        elevation: 0.0,
                        color: Color.fromRGBO(107, 152, 71, 1),
                        textColor: Color.fromRGBO(27, 28, 28, 1),
                        disabledTextColor: Colors.white10,
                        // acción a realizar cuando se presiona el boton de login
                        onPressed: () async {
                          if (nombre == '') {
                            nombre = f['nombre'];
                          }
                          if (descripcion == '') {
                            descripcion = f['descripcion'];
                          }
                          if (foto == '') {
                            foto = f['foto'];
                          }
                          if (precio == '') {
                            precio = f['precio'].toString();
                          }

                          try {
                            final res = await DBProvider.db.modifyProducto(
                                nombre,
                                descripcion,
                                foto,
                                int.parse(precio),
                                f['id']);
                            if (res != null) {
                              Navigator.pop(context);
                              showAlert(context, 'Se ha modificado el producto',
                                  '¡Felicidades!', 'assets/cactus(1).png');
                            } else {}
                          } catch (e) {}
                        }),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
