import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';
import 'package:flutter/material.dart';

class ConfirmarCompra extends StatefulWidget {
  ConfirmarCompra({Key key}) : super(key: key);

  @override
  _ConfirmarCompraState createState() => _ConfirmarCompraState();
}

class _ConfirmarCompraState extends State<ConfirmarCompra> {
  String metodo = 'Tarjeta de Credito';
  String direccion;
  final UserPrefs prefs = new UserPrefs();
  Size s;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    final id_producto = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de jenny", "assets/cactus(1).png");
          }),
      backgroundColor: Color.fromRGBO(228, 214, 124, 1),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Image.asset('assets/tenor.gif'),
            Container(
              width: s.width * 0.9,
              padding: EdgeInsets.all(8),
              color: Colors.white10,
              child: TextFormField(
                keyboardType: TextInputType.text,
                decoration: InputDecoration(
                    hintText: 'Ingrese la dirección', helperText: 'Dirección'),
                onChanged: (value) {
                  direccion = value;
                },
              ),
            ),
            Container(
              alignment: Alignment.topLeft,
              padding: EdgeInsets.symmetric(
                  horizontal: s.width * 0.07, vertical: s.height * 0.01),
              child: Text('Seleccione un metodo de pago'),
            ),
            Container(
              padding: EdgeInsets.symmetric(horizontal: s.width * 0.02),
              alignment: Alignment.center,
              width: s.width * 0.9,
              child: DropdownButton(
                  value: metodo,
                  items: [
                    DropdownMenuItem(
                      child: Text('Tarjeta de Credito'),
                      value: 'Tarjeta de Credito',
                    ),
                    DropdownMenuItem(
                      child: Text('Efectivo'),
                      value: 'Efectivo',
                    ),
                    DropdownMenuItem(
                      child: Text('PSE'),
                      value: 'PSE',
                    )
                  ],
                  onChanged: (value) {
                    setState(() {
                      metodo = value.toString();
                    });
                  }),
            ),
            SizedBox(
              height: s.height * 0.05,
            ),
            Container(
              width: s.width * 0.5,
              height: s.height * 0.07,
              child: RaisedButton(
                  child: Container(
                    padding: EdgeInsets.symmetric(
                        horizontal: s.width * 0.1, vertical: s.height * 0.01),
                    //texto que se muestra en el boton
                    child: Text(
                      'Confirmar',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0)),
                  elevation: 0.0,
                  color: Color.fromRGBO(107, 152, 71, 1),
                  textColor: Color.fromRGBO(27, 28, 28, 1),
                  disabledTextColor: Colors.white10,
                  // acción a realizar cuando se presiona el boton de login
                  onPressed: () async {
                    try {
                      final res = await DBProvider.db.nuevoPedido(
                          int.parse(prefs.id),
                          int.parse(id_producto),
                          metodo,
                          direccion);
                      if (res != null) {
                        Navigator.pushNamedAndRemoveUntil(context, 'cliente',
                            (Route<dynamic> route) => false);
                      }
                    } catch (e) {}
                  }),
            )
          ],
        ),
      ),
    );
  }
}
