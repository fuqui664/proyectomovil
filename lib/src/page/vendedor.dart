import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jenny/src/utils/utils.dart';

class VendedorPage extends StatefulWidget {
  const VendedorPage({Key key}) : super(key: key);

  @override
  _VendedorPageState createState() => _VendedorPageState();
}

class _VendedorPageState extends State<VendedorPage> {
  Size s;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        floatingActionButton: IconButton(
            icon: Icon(Icons.code),
            onPressed: () {
              showAlert2(
                  context, "", "Acerca de DessertApp", "assets/cactus(1).png");
            }),
        backgroundColor: Color.fromRGBO(228, 214, 124, 1),
        body: SafeArea(
          child: Center(
              child: Stack(
            alignment: Alignment.center,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0, 0, 0, s.height * 0.45),
                child: Image.asset('assets/tenor.gif'),
              ),
              Center(
                child: Container(
                  margin: EdgeInsets.fromLTRB(0, s.height * 0.2, 0, 0),
                  decoration: BoxDecoration(
                      // color: Color.fromRGBO(107, 152, 71, 0.6),
                      borderRadius: BorderRadius.circular(10)),
                  height: s.height * 0.21,
                  width: s.width * 0.45,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: <Widget>[
                      FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: () {
                          Navigator.of(context).pushNamed('productos');
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: s.width * 0.9,
                          height: s.height * 0.06,
                          child: Text(
                            'Productos',
                            style: TextStyle(
                              color: Color.fromRGBO(27, 28, 28, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        color: Color.fromRGBO(107, 152, 71, 1),
                      ),
                      FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: () {
                          Navigator.of(context).pushNamed('nuevop');
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: s.width * 0.9,
                          height: s.height * 0.06,
                          child: Text(
                            'Agregar Producto',
                            style: TextStyle(
                              color: Color.fromRGBO(27, 28, 28, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        color: Color.fromRGBO(107, 152, 71, 1),
                      ),
                      FlatButton(
                        shape: new RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: () {
                          Navigator.of(context).pushNamed('pedidos');
                        },
                        child: Container(
                          alignment: Alignment.center,
                          width: s.width * 0.9,
                          height: s.height * 0.06,
                          child: Text(
                            'Pedidos',
                            style: TextStyle(
                              color: Color.fromRGBO(27, 28, 28, 1),
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        color: Color.fromRGBO(107, 152, 71, 1),
                      )
                    ],
                  ),
                ),
              )
            ],
          )),
        ),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            title: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  height: s.height * 0.14,
                  child: Image(
                    image: AssetImage('assets/logout.png'),
                  ),
                ),
                SizedBox(height: s.height * 0.03),
              ],
            ),
            content: Text(
              '¿Que deseas hacer?',
              style: TextStyle(fontSize: 16),
              textAlign: TextAlign.center,
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'Cerrar sesión',
                ),
                onPressed: () {
                  Navigator.pushNamedAndRemoveUntil(
                      context, 'init', (Route<dynamic> route) => false);
                },
              ),
              // SizedBox(width: 15,),
              FlatButton(
                child: Text(
                  'Salir de la aplicacion',
                  style: TextStyle(color: Colors.red),
                ),
                onPressed: () => exit(0),
              ),
            ],
          );
        });
  }
}
