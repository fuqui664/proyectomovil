import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';

class ProducosPage extends StatefulWidget {
  ProducosPage({Key key}) : super(key: key);

  @override
  _ProducosPageState createState() => _ProducosPageState();
}

class _ProducosPageState extends State<ProducosPage> {
  Size s;
  UserPrefs prefs = UserPrefs();
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    return Scaffold(
        floatingActionButton: IconButton(
            icon: Icon(Icons.code),
            onPressed: () {
              showAlert2(
                  context, "", "Acerca de DessertApp", "assets/cactus(1).png");
            }),
        appBar: AppBar(
          backgroundColor: Color.fromRGBO(228, 214, 124, 1),
          title: Text(
            'DessertApp',
            style: TextStyle(color: Color.fromRGBO(27, 28, 28, 1)),
          ),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Image.asset('assets/cactus(1).png'),
            )
          ],
        ),
        body: FutureBuilder<List>(
          future: DBProvider.db.getResultsById(prefs.id),
          // initialData: InitialData,
          builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
            if (!snapshot.hasData) {
              return Center(
                child: CupertinoActivityIndicator(),
              );
            }
            final results = snapshot.data;
            if (results.length == 0) {
              return Column(
                // crossAxisAlignment: CrossAxisAlignment.center,
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    height: s.height * 0.03,
                  ),
                  Center(
                    child: Card(
                      margin: EdgeInsets.all(15),
                      child: Container(
                          alignment: Alignment.center,
                          height: s.height * 0.765,
                          child: Text("No hay  productos")),
                    ),
                  )
                ],
              );
            }
            return SafeArea(
              child: Column(
                children: <Widget>[
                  Flexible(
                    child: ListView.builder(
                      itemBuilder: (context, i) {
                        return GestureDetector(
                          child: Card(
                              elevation: 5,
                              margin: EdgeInsets.fromLTRB(20, 10, 20, 10),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Container(
                                    // height: s.height * 0.1,
                                    width: s.width * 0.2,
                                    padding: EdgeInsets.all(8),
                                    child: Image.network(results[i]["foto"]),
                                  ),
                                  Container(
                                    width: s.width * 0.005,
                                    color: Colors.grey,
                                    height: s.height * 0.1,
                                  ),
                                  Column(
                                    children: <Widget>[
                                      Row(
                                        children: <Widget>[
                                          Container(
                                            padding: EdgeInsets.all(8),
                                            child: Text(
                                              results[i]['descripcion'],
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      27, 28, 28, 1)),
                                            ),
                                            width: s.width * 0.55,
                                          ),
                                        ],
                                      ),
                                      Container(
                                        width: s.width * 0.55,
                                        color: Colors.grey,
                                        height: s.height * 0.001,
                                      ),
                                      Row(
                                        children: <Widget>[
                                          SizedBox(
                                            width: s.width * 0.01,
                                          ),
                                          Container(
                                              padding: EdgeInsets.all(5),
                                              child: Text('Precio')),
                                          Container(
                                            color: Colors.black12,
                                            padding: EdgeInsets.all(5),
                                            child:
                                                Text('${results[i]["precio"]}'),
                                          ),
                                          SizedBox(
                                            width: s.width * 0.09,
                                          ),
                                          FlatButton(
                                              color: Color.fromRGBO(
                                                  107, 152, 71, 1),
                                              onPressed: () {
                                                Navigator.pushNamed(
                                                    context, 'modify',
                                                    arguments: results[i]);
                                              },
                                              child: Text('Modificar'))
                                        ],
                                      ),
                                    ],
                                  )
                                ],
                              )),
                          onTap: () {
                            Navigator.pushNamed(context, 'detalles',
                                arguments: results[i]);
                          },
                        );
                      },
                      itemCount: results.length,
                    ),
                  )
                ],
              ),
            );
          },
        ));
  }
}
