
import 'package:flutter/material.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/utils/utils.dart';

class RegistroPage extends StatefulWidget {
  RegistroPage({Key key}) : super(key: key);

  @override
  _RegistroPageState createState() => _RegistroPageState();
}

class _RegistroPageState extends State<RegistroPage> {
  String nombre = '';
  String telefono = '';
  String contrasena = '';
  Size s;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de DessertApp", "assets/cactus(1).png");
          }),
      appBar: AppBar(
        backgroundColor: Color.fromRGBO(228, 214, 124, 1),
        title: Text(
          'DesertApp',
        ),
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Image.asset('assets/cactus(1).png'),
          )
        ],
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            Container(
              height: s.height * 0.9,
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(8),
                      height: s.height * 0.3,
                      child: Image.asset('assets/cactus(1).png')),
                  Container(
                    child: Text(
                      'Para registrarte necesitas los siguientes datos:',
                      style: Theme.of(context)
                          .textTheme
                          .headline4
                          .apply(fontSizeFactor: 0.65),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    width: s.width * 0.9,
                    child: TextField(
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        setState(() {
                          telefono = value;
                        });
                      },
                      decoration: InputDecoration(
                          hintText: 'Telefono', labelText: 'Telefono'),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(vertical: 8),
                    width: s.width * 0.9,
                    child: TextField(
                      onChanged: (value) {
                        setState(() {
                          nombre = value;
                        });
                      },
                      decoration: InputDecoration(
                          hintText: 'Nombre', labelText: 'Nombre'),
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    padding: EdgeInsets.symmetric(vertical: 8),
                    child: TextField(
                      obscureText: true,
                      onChanged: (value) {
                        setState(() {
                          contrasena = value;
                        });
                      },
                      decoration: InputDecoration(
                          hintText: 'Contraseña', labelText: 'Contraseña'),
                    ),
                  ),
                  Container(
                    width: s.width * 0.5,
                    height: s.height * 0.07,
                    margin: EdgeInsets.only(top: 20),
                    child: RaisedButton(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: s.width * 0.05,
                              vertical: s.height * 0.01),
                          //texto que se muestra en el boton
                          child: Text(
                            'Registrarme',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        elevation: 0.0,
                        color: Color.fromRGBO(107, 152, 71, 1),
                        textColor: Color.fromRGBO(27, 28, 28, 1),
                        disabledTextColor: Colors.white10,
                        // acción a realizar cuando se presiona el boton de login
                        onPressed: () async {
                          if (nombre == '' ||
                              telefono == '' ||
                              contrasena == '') {
                            showAlert(
                                context,
                                'Debes ingresar los datos para poder registrarte',
                                '¡Espera!',
                                'assets/desert.png');
                          } else {
                            try {
                              final res = await DBProvider.db
                                  .nuevoRegistro(nombre, telefono, contrasena);
                              if (res != null) {
                                Navigator.pushNamedAndRemoveUntil(context,
                                    'login', (Route<dynamic> route) => false);
                                showAlert(
                                    context,
                                    'Tu registro se ha completado con exito',
                                    '¡Felicidades!',
                                    'assets/cactus(1).png');
                              }
                            } catch (e) {}
                          }
                        }),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
