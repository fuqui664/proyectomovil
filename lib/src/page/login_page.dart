import 'package:jenny/src/properties/validations.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// import 'package:uwheels_app/src/bloc/patron_bloc.dart';
// import 'package:uwheels_app/src/bloc/user_data.dart';
// import 'package:uwheels_app/src/bloc/patron_bloc.dart';
import 'dart:io';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Size s;
  final _formKey = GlobalKey<FormState>();

  // ProgressDialog progressDialog;
  String _email = '';
  String _password = '';
  UserPrefs prefs = new UserPrefs();
  @override
  Widget build(BuildContext context) {
    // bloc = ProviderLogin.of(context);
    //progressDialog = ProgressDialog(context, type: ProgressDialogType.Normal);
    s = MediaQuery.of(context).size;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de jenny", "assets/cactus(1).png");
          }),
      backgroundColor: Color.fromRGBO(228, 214, 124, 1),
      body: SingleChildScrollView(child: _fondo(context)),
    );
  }

  Widget _fondo(BuildContext context) {
    //TODO: Verificar Imagen en Dispositivo Grande
    return Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Container(
          margin: EdgeInsets.fromLTRB(0, 0, 0, s.height * 0.35),
          child: Image.asset('assets/tenor.gif'),
        ),
        _inputs(context),
      ],
    );
  }

  Widget _inputs(BuildContext context) {
    return Center(
      child: Container(
        margin: EdgeInsets.only(top: s.height * 0.53, bottom: 10),
        //height: s.height * 0.,
        width: s.width * 0.85,
        // formulario
        child: Form(
          // key de formulario
          key: _formKey,
          child: Column(
            // mainAxisSize: MainAxisSize.min,
            // crossAxisAlignment: CrossAxisAlignment.center,

            children: <Widget>[
              SizedBox(height: s.height * 0.017),
              _ingresarEmail(context),
              SizedBox(height: s.height * 0.017),

              _ingresarPassword(context),

              SizedBox(height: s.height * 0.03),
              // boton de login y otros botones mostrados luego de los campos de texto
              _bottom(context),
            ],
          ),
        ),
      ),
    );
  }

  // campo de texto del correo
  Widget _ingresarEmail(BuildContext context) {
    return TextFormField(
        // tipo de teclado que se mostrara al usar el campo de texto
        keyboardType: TextInputType.emailAddress,
        //key: _formKey,
        cursorColor: Colors.grey,
        autofocus: false,

        //autovalidate: true,
        // validación de la entrada del usuario
        onChanged: (value) {
          setState(() {
            _email = value;
          });
        },
        validator: (value) {
          if (value.length == 0) {
            // texto a mostrar cuando la validación no esta bien
            return "Debe ingresar un usuario";
          }
        },
        style: TextStyle(
          color: Color.fromRGBO(27, 28, 28, 1),
        ),
        decoration: InputDecoration(
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          // texto mostrado por defecto en el campo de texto
          hintText: 'Usuario',

          hintStyle: TextStyle(
              // fontSize: 15.0,
              fontWeight: FontWeight.w700,
              // height: 0.7,,
              color: Color.fromRGBO(27, 28, 28, 1)),
        ));
  }

  // campo de texto de la contraseña
  Widget _ingresarPassword(BuildContext context) {
    return TextFormField(
        // para oxultar la contraseña al ser ingresada
        obscureText: true,
        autofocus: false,
        // expands: false,
        keyboardType: TextInputType.text,
        cursorColor: Colors.grey,
        //autovalidate: true,
        // validación de la entrada del usuario

        validator: (value) {
          if (value.length < 6) {
            // texto de error cuando la validación no es exitosa
            return "La contraseña debe tener almenos 6 caracteres";
          }
        },
        onChanged: (value) {
          setState(() {
            _password = value;
          });
        },
        style: TextStyle(
          color: Color.fromRGBO(27, 28, 28, 1),
        ),
        decoration: InputDecoration(
          border: OutlineInputBorder(borderRadius: BorderRadius.circular(10.0)),
          // texto a mostrar como predeterminado
          hintText: 'Contraseña',
          contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
          hintStyle: TextStyle(
            // fontSize: 15.0,
            fontWeight: FontWeight.w700,
            // height: 0.8,
            color: Color.fromRGBO(27, 28, 28, 1),
          ),
        ));
  }

  // boton de login
  Widget _botonLogin(BuildContext context) {
    return Container(
      width: s.width * 0.5,
      height: s.height * 0.07,
      child: RaisedButton(
          child: Container(
            padding: EdgeInsets.symmetric(
                horizontal: s.width * 0.1, vertical: s.height * 0.01),
            //texto que se muestra en el boton
            child: Text(
              'Ingresar',
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 16.0,
              ),
            ),
          ),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 0.0,
          color: Color.fromRGBO(107, 152, 71, 1),
          textColor: Color.fromRGBO(27, 28, 28, 1),
          disabledTextColor: Colors.white10,
          // acción a realizar cuando se presiona el boton de login
          onPressed: () {
            FocusScopeNode currentFocus = FocusScope.of(context);

            // para quitar el focus de los campos de texto
            currentFocus.unfocus();
            if (_formKey.currentState.validate()) {
              _login(context);
            }

            //
          }),
    );
  }

  _login(BuildContext context) async {
    loadings(context);
    try {
      final result = await DBProvider.db.getUser(_email, _password);
      if (result != null) {
        Navigator.pop(context);
        List results = result.values.toList();

        if (results[1] == 2) {
          prefs.id = results[0].toString();
          Navigator.pushNamedAndRemoveUntil(
              context, 'vendedor', (Route<dynamic> route) => false);
        } else {
          Navigator.pop(context);
          showAlert(context, 'Usuario o contraseña incorrectos',
              '¡Opps, ha ocurrido un error!', 'assets/desert.png');
        }
      } else {
        Navigator.pop(context);
        showAlert(context, 'Usuario o contraseña incorrectos',
            '¡Opps, ha ocurrido un error!', 'assets/desert.png');
      }
      // loadings(context);

    } catch (e) {}
  }

  Widget _bottom(BuildContext context) {
    return Center(
      child: Container(
        // margin: EdgeInsets.only( top: s.height * 0.8),
        width: s.width * 0.85,
        height: s.height * 0.2,
        child: Column(
          // lista de widgets mostrados
          children: <Widget>[
            //SizedBox(height: s.height*0.001,),

            _botonLogin(context),
            SizedBox(
              height: s.height * 0.03,
            ),
            // _registro(context),
            // _recuperarContra(context),
          ],
        ),
      ),
    );
  }

  // widget de texto de registro
  Widget _registro(BuildContext context) {
    return Container(
      width: s.width * 0.85,
      height: s.height * 0.04,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // texto mostrado
          Text(
            '¿Aún no estás registrado?',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14.0,
              color: Colors.black,
            ),
          ),
          // boton que redirige a la pagina de registro
          FlatButton(
              //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                // teto dentro del boton
                'Hazlo ahora',
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0,
                    color: Color.fromRGBO(107, 152, 71, 1)),
              ),
              textColor: Colors.black,
              // redirección a la pagina pregunta si desea registrar un vehiculo
              onPressed: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                // para quitar el focus de los campos de texto
                currentFocus.unfocus();
              }),
        ],
      ),
    );
  }

  // widget que pregunta al usuario si olvido su contraseña
  Widget _recuperarContra(BuildContext context) {
    return Container(
      width: s.width * 0.85,
      height: s.height * 0.04,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          // texto mostrado en el widget
          Text(
            '¿Has olvidado tu contraseña?',
            style: TextStyle(
              fontWeight: FontWeight.w400,
              fontSize: 14.0,
              color: Colors.black,
            ),
          ),
          // boton que redirige a la pagina de recuperacion de contraseña
          FlatButton(
              //padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
              child: Text(
                // texto dentro del boton
                'Recupérala',
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 14.0,
                    color: Color.fromRGBO(107, 152, 71, 1)),
              ),
              textColor: Colors.black,
              // redirección a la pagina de recuperacion de contraseña
              onPressed: () {
                FocusScopeNode currentFocus = FocusScope.of(context);

                // para quitar el focus de los campos de texto
                currentFocus.unfocus();
              }),
        ],
      ),
    );
  }
}
