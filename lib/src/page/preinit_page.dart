import 'dart:io';


import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';

class PreInitPage extends StatefulWidget {
  const PreInitPage({Key key}) : super(key: key);

  @override
  _PreInitPageState createState() => _PreInitPageState();
}

class _PreInitPageState extends State<PreInitPage> {
  UserPrefs prefs = new UserPrefs();
  Size s;
  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    s = size;
    return WillPopScope(
        child: Scaffold(
          floatingActionButton: IconButton(
              icon: Icon(Icons.code),
              onPressed: () {
                showAlert2(context, "", "Acerca de DessertApp",
                    "assets/cactus(1).png");
              }),
          backgroundColor: Color.fromRGBO(228, 214, 124, 1),
          body: SafeArea(
            child: Center(
                child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.fromLTRB(0, 0, 0, size.height * 0.35),
                  child: Image.asset('assets/tenor.gif'),
                ),
                Center(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(0, size.height * 0.2, 0, 0),
                    decoration: BoxDecoration(
                        // color: Color.fromRGBO(107, 152, 71, 0.6),
                        borderRadius: BorderRadius.circular(10)),
                    height: size.height * 0.21,
                    width: size.width * 0.45,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        FlatButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0)),
                          onPressed: () {
                            prefs.id = '3';
                            Navigator.of(context).pushNamed('cliente');
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: size.width * 0.9,
                            height: size.height * 0.06,
                            child: Text(
                              'Clientes',
                              style: TextStyle(
                                color: Color.fromRGBO(27, 28, 28, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                          color: Color.fromRGBO(107, 152, 71, 1),
                        ),
                        FlatButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(10.0)),
                          onPressed: () {
                            Navigator.pushNamed(context, 'init2');
                          },
                          child: Container(
                            alignment: Alignment.center,
                            width: size.width * 0.9,
                            height: size.height * 0.06,
                            child: Text(
                              'Vendedores',
                              style: TextStyle(
                                color: Color.fromRGBO(27, 28, 28, 1),
                                fontWeight: FontWeight.bold,
                                fontSize: 16.0,
                              ),
                            ),
                          ),
                          color: Color.fromRGBO(107, 152, 71, 1),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )),
          ),
        ),
        onWillPop: _onBackPressed);
  }

  Future<bool> _onBackPressed() {
    return showCupertinoDialog(
        context: context,
        builder: (context) {
          return CupertinoAlertDialog(
            // shape: RoundedRectangleBorder(
            //   borderRadius: BorderRadius.circular(20.0),
            // ),
            title: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text(
                  '¿Quieres salir de la aplicación?',
                  style: TextStyle(),
                  textAlign: TextAlign.center,
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                // color: Colors.purple,
                child: Container(
                    alignment: Alignment.center,
                    height: s.height * 0.06,
                    width: s.width * 0.16,
                    child: Text(
                      'Cancelar',
                      style: TextStyle(
                          color: Colors.red, fontWeight: FontWeight.w600),
                    )),
                onPressed: () => Navigator.pop(context, false),
              ),
              FlatButton(
                // color: Colors.red,
                // shape: new RoundedRectangleBorder(
                //     borderRadius: new BorderRadius.circular(12.0)),
                child: Container(
                  alignment: Alignment.center,
                  height: s.height * 0.06,
                  width: s.width * 0.16,
                  child: Text(
                    'Aceptar',
                    style: TextStyle(
                        color: Color.fromRGBO(107, 152, 71, 1),
                        fontWeight: FontWeight.w600),
                  ),
                ),
                onPressed: () => exit(0),
              ),
            ],
          );
        });
  }
}
