
import 'package:flutter/material.dart';
import 'package:jenny/src/providers/db_provider.dart';
import 'package:jenny/src/shredprefs/user_prefs.dart';
import 'package:jenny/src/utils/utils.dart';

class NuevoPage extends StatefulWidget {
  NuevoPage({Key key}) : super(key: key);

  @override
  _NuevoPageState createState() => _NuevoPageState();
}

class _NuevoPageState extends State<NuevoPage> {
  String descripcion = '';
  String nombre = '';
  String foto = '';
  String precio = '';
  final UserPrefs prefs = new UserPrefs();
  Size s;
  @override
  Widget build(BuildContext context) {
    s = MediaQuery.of(context).size;
    // final id_producto = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      floatingActionButton: IconButton(
          icon: Icon(Icons.code),
          onPressed: () {
            showAlert2(
                context, "", "Acerca de DessertApp", "assets/cactus(1).png");
          }),
      backgroundColor: Color.fromRGBO(228, 214, 124, 1),
      body: SafeArea(
        child: ListView(
          children: <Widget>[
            Container(
              height: s.height,
              child: Column(
                children: <Widget>[
                  Image.asset('assets/tenor.gif'),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: 'Ingrese el nombre', helperText: 'Nombre'),
                      onChanged: (value) {
                        nombre = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: 'Ingrese la descripcion',
                          helperText: 'Descripción'),
                      onChanged: (value) {
                        descripcion = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.text,
                      decoration: InputDecoration(
                          hintText: 'Ingrese la url de la imagen',
                          helperText: 'Imagen'),
                      onChanged: (value) {
                        foto = value;
                      },
                    ),
                  ),
                  Container(
                    width: s.width * 0.9,
                    // padding: EdgeInsets.all(8),
                    color: Colors.white10,
                    child: TextFormField(
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                          hintText: 'Ingrese el precio', helperText: 'Precio'),
                      onChanged: (value) {
                        precio = value;
                      },
                    ),
                  ),
                  SizedBox(
                    height: s.height * 0.03,
                  ),
                  Container(
                    width: s.width * 0.5,
                    height: s.height * 0.07,
                    child: RaisedButton(
                        child: Container(
                          padding: EdgeInsets.symmetric(
                              horizontal: s.width * 0.1,
                              vertical: s.height * 0.01),
                          //texto que se muestra en el boton
                          child: Text(
                            'Agregar',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0,
                            ),
                          ),
                        ),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0)),
                        elevation: 0.0,
                        color: Color.fromRGBO(107, 152, 71, 1),
                        textColor: Color.fromRGBO(27, 28, 28, 1),
                        disabledTextColor: Colors.white10,
                        // acción a realizar cuando se presiona el boton de login
                        onPressed: () async {
                          if (nombre == '' ||
                              descripcion == '' ||
                              foto == '' ||
                              precio == '') {
                            showAlert(
                                context,
                                'Debes ingresar los datos para poder registrar el producto',
                                '¡Espera!',
                                'assets/desert.png');
                          } else {
                            try {
                              final res = await DBProvider.db.nuevoProducto(
                                  nombre,
                                  descripcion,
                                  foto,
                                  int.parse(precio),
                                  prefs.id);
                              if (res != null) {
                                Navigator.pop(context);
                                showAlert(
                                    context,
                                    'Se ha registrado el producto',
                                    '¡Felicidades!',
                                    'assets/cactus(1).png');
                              } else {}
                            } catch (e) {}
                          }
                        }),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
