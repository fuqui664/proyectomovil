import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

class DBProvider {
  static Database _database;
  static final DBProvider db = DBProvider._();

  DBProvider._();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    } else {
      _database = await initDB();
      return _database;
    }
  }

  // inicializar la base de datos
  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'Info.db');
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute('CREATE TABLE Users ('
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          'rol INTEGER,'
          'usuario TEXT,'
          'contraseña TEXT,'
          'telefono TEXT'
          ')');
      await db.execute('CREATE TABLE Pedidos ('
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          'iduser INTEGER,'
          'idproducto INTEGER,'
          'metodo TEXT,'
          'direccion TEXT'
          ')');
      await db.execute('CREATE TABLE Productos ('
          "id INTEGER PRIMARY KEY AUTOINCREMENT,"
          'precio INTEGER,'
          'nombre TEXT,'
          'foto TEXT,'
          'descripcion TEXT,'
          'estado TEXT,'
          'usuario TEXT'
          ')');
      await db
          .rawInsert("INSERT Into Users ( rol, usuario, contraseña, telefono ) "
              "VALUES ( 1, 'admin', '123456', 'telefono' )");
      await db
          .rawInsert("INSERT Into Users ( rol, usuario, contraseña, telefono ) "
              "VALUES ( 2, 'vendedor', '123456', 'telefono' )");
      await db
          .rawInsert("INSERT Into Users ( rol, usuario, contraseña, telefono ) "
              "VALUES ( 3, 'cliente_defecto', '123456', 'telefono' )");

      await db.rawInsert(
          "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
          "VALUES (2000, 'Tarta de limon', 'https://okdiario.com/img/2018/06/09/tarta-de-limon-655x368.jpg', 'Tarta hecha de crema y limon', 'disponible', '2')");

      await db.rawInsert(
          "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
          "VALUES (1000, 'Deditos de queso', 'https://www.mycolombianrecipes.com/wp-content/uploads/2012/09/Deditos-o-Palitos.jpg', '', 'disponible', '2')");
      await db.rawInsert(
          "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
          "VALUES (200, 'Gomitas trululu', 'https://jumbocolombiafood.vteximg.com.br/arquivos/ids/3493718-1000-1000/7702993028261.jpg?v=637218884116170000', 'Gomitas trululu aros', 'disponible', '2')");
      await db.rawInsert(
          "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
          "VALUES (1500, 'Postre de 3 leches', 'https://img-global.cpcdn.com/recipes/cb898527fce80dd9/1200x630cq70/photo.jpg', 'Poste de 3 leches hecho en casa', 'disponible', '2')");
      await db.rawInsert(
          "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
          "VALUES (1500, 'Empanadas', 'https://www.laopinion.com.co/sites/default/files/2019/02/22/imagen/empanada.jpg', 'Empanadas rellenas de queso y carne molida', 'disponible', '2')");
    });
  }

  // CREAR registros
  nuevoRegistro(String nombre, String telefono, String contrasena) async {
    final db = await database;
    final res = await db
        .rawInsert("INSERT Into Users ( rol, usuario, contraseña, telefono ) "
            "VALUES ( 2, '$nombre', '$contrasena', '$telefono' )");
    return res;
  }

  nuevoProducto(String nombre, String descripcion, String foto, int precio,
      String id) async {
    final db = await database;
    final res = await db.rawInsert(
        "INSERT Into Productos ( precio, nombre, foto, descripcion, estado , usuario) "
        "VALUES ( '$precio', '$nombre', '$foto', '$descripcion','disponible', '$id' )");
    return res;
  }

  modifyProducto(String nombre, String descripcion, String foto, int precio,
      int id) async {
    final db = await database;
    final res = await db.rawInsert(
        "UPDATE Productos SET precio='$precio', nombre='$nombre', foto='$foto', descripcion='$descripcion' WHERE id =$id");
    return res;
  }

  nuevoPedido(
      int id_user, int id_producto, String metodo, String direccion) async {
    final db = await database;
    final res = await db.rawInsert(
        "INSERT Into Pedidos ( iduser, idproducto, metodo, direccion ) "
        "VALUES ( $id_user, $id_producto, '$metodo', '$direccion')");
    return res;
  }

  Future<Map> getUser(String user, String password) async {
    final db = await database;
    final res = await db.query('Users',
        where: 'usuario = ? AND contraseña = ?', whereArgs: [user, password]);
    return res.isNotEmpty ? res.first : null;
  }

  Future<List> getResults() async {
    final db = await database;
    final res = await db.query('Productos', columns: [
      'id',
      'precio',
      'nombre',
      'foto',
      'descripcion',
      'estado',
      'usuario'
    ]);
    List productos = res.isNotEmpty ? res.map((e) => e).toList() : [];
    return productos;
  }
// obtener el producto con el id
  Future<List> getResultsById(String query) async {
    final db = await database;
    final res =
        await db.query('Productos', where: 'usuario = ?', whereArgs: [query]);
    List productos = res.isNotEmpty ? res.map((e) => e).toList() : [];
    return productos;
  }

  Future<Map> getProducto(int id) async {
    final db = await database;
    final res = await db.query('Productos', where: 'id = ?', whereArgs: [
      id
    ], columns: [
      'id',
      'precio',
      'nombre',
      'foto',
      'descripcion',
      'estado',
      'usuario'
    ]);
    return res.isNotEmpty ? res.first : null;
  }

  Future<List> getPedidos() async {
    final db = await database;
    final res = await db.query('Pedidos', columns: [
      'id',
      'iduser',
      'idproducto',
      'metodo',
      'direccion',
    ]);
    List productos = res.isNotEmpty ? res.map((e) => e).toList() : [];
    return productos;
  }

  Future<Map> getUserById(int id) async {
    final db = await database;
    final res = await db.query('Users', where: 'id = ?', whereArgs: [id]);

    return res.isNotEmpty ? res.first : null;
  }

  delResults() async {
    final db = await database;
    final res = await db.delete('Results');
    return res;
  }
}
