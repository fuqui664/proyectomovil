import 'package:shared_preferences/shared_preferences.dart';

class UserPrefs {
  //ususario y contraseña para hacer el login auto
  static final UserPrefs _instancia = new UserPrefs._internal();

  factory UserPrefs() {
    return _instancia;
  }
  UserPrefs._internal();

  SharedPreferences prefs;

  initPrefs() async {
    prefs = await SharedPreferences.getInstance();
  }
  //getters y setters

  set id(String id) {
    prefs.setString('id', id);
  }

  set username(String arg) {
    prefs.setString('username', arg);
  }

  get id {
    return prefs.getString('id');
  }
}
